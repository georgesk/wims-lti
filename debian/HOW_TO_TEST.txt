To run a test of the packages wims-lti, here are the steps:
===========================================================

ensure wims is installed and that http://127.0.0.1/wims/wims.cgi is OK.

run the followong commands in a terminal:
-----------------------------------------

quilt push -a
export WIMS_URL=http://127.0.0.1/wims/wims.cgi
export WIMSAPI_TIMEOUT=40
sudo -u wims --preserve-env=WIMS_URL,WIMSAPI_TIMEOUT -s \
   python3 ./manage.py test --failfast

sudo -u wims -s /bin/bash -c "find /var/lib/wims/log/classes -maxdepth 1 -mtime -2 | grep -E './.*[0-9][0-9][0-9][0-9][0-9][0-9]'| xargs rm -fr"
